# legend_tools

Tools for interoperability with file formats used in the game "Mission Critical", but can be adapted for others games by Legend Entertainment.

For more context, read the [accompanying writeup](https://qufb.gitlab.io/writeups/2021/01/22/Decrushing-DOS-Doodads).

Disclaimer: This project is not associated with Legend Entertainment or its current copyright owners.
