#!/usr/bin/env python3

from PIL import Image
from pathlib import Path
import argparse
import logging
import numpy

# Constants
METADATA_HEADER_LEN = 0xA
METADATA_WIDTHS_COUNT = 0x5F

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s %(filename)s:%(lineno)s:%(funcName)s] %(message)s",
)


def convert_fnt(filename, max_width, char_structs):
    data = numpy.zeros((char_height, max_width, 3), dtype=numpy.uint8)
    width_acc = 0
    for i, c in enumerate(char_structs):
        for j, v in enumerate(c["data"]):
            for k in range(c["width"]):
                if k >= len(v):
                    data[j, width_acc + k] = [0] * 3
                else:
                    data[j, width_acc + k] = [int(v[k]) * 0xFF] * 3
        width_acc += c["width"]

    image = Image.fromarray(data)
    image.save(f"{filename}.png")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data", type=str, help="Path for data file (e.g. MC001.FNT)")
    parser.add_argument("convert_dir", type=str, help="Path for converted file")
    parsed_args = parser.parse_args()

    file = parsed_args.data
    convert_dir = Path(parsed_args.convert_dir)
    convert_dir.mkdir(parents=True, exist_ok=True)
    converted_file = (convert_dir / Path(file)).absolute()

    input_bytes = None
    with open(file, "rb") as f:
        input_bytes = bytearray(f.read())

    # A row of pixels is encoded in these many bytes
    char_width_len = input_bytes[0x2]

    metadata_widths_start_offset = METADATA_HEADER_LEN + input_bytes[0x3]
    metadata_widths_end_offset = metadata_widths_start_offset + METADATA_WIDTHS_COUNT
    widths = input_bytes[metadata_widths_start_offset:metadata_widths_end_offset]
    logging.debug(
        f"widths: [{hex(metadata_widths_start_offset)}:{hex(metadata_widths_end_offset)}]"
    )

    char_height = input_bytes[0x6]
    logging.debug(f"char size: w={hex(char_width_len*8)}, h={hex(char_height)}")

    if input_bytes[0x8] == 0xFF:
        pixmap_start_pad = 0x80  # Padding to skip over the spacings section
        spacings = input_bytes[0xAA : 0xAA + METADATA_WIDTHS_COUNT]
    else:
        pixmap_start_pad = 0
        spacings = [0] * METADATA_WIDTHS_COUNT

    total_width = 0
    chars = []
    for i in range(METADATA_WIDTHS_COUNT):
        widths_offset = metadata_widths_start_offset + i
        pixmap_offset = (
            pixmap_start_pad
            + metadata_widths_end_offset
            + (i * (char_height * char_width_len))
        )
        char_width = input_bytes[widths_offset] + (spacings[i] & 1)
        total_width += char_width
        logging.debug(
            f"char {i}: wo={hex(widths_offset)}, po={hex(pixmap_offset)}, w={hex(char_width)}"
        )
        pixmap_data = []
        for j in range(0, char_width_len * char_height, char_width_len):
            row_data = []
            for k in range(0, char_width_len):
                partial_row_data = input_bytes[pixmap_offset + j + k]
                row_data += [*bin(partial_row_data)[2:].zfill(8)]
            pixmap_data.append(row_data)
        char_struct = {"width": char_width, "data": pixmap_data}
        chars.append(char_struct)

    convert_fnt(converted_file, total_width, chars)
