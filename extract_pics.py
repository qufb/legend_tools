#!/usr/bin/env python3

from PIL import Image
from pathlib import Path
import argparse
import copy
import logging
import numpy
import re
import sys
import struct
import subprocess

# Constants
KNOWN_ENTRIES = {  # FIXME: Multiple sizes missing
    "bg": {"w": 640, "h": 288},
    "arrow_item": {"w": 20, "h": 48},
    "item": {"w": 50, "h": 48},
    "cursor_wait": {"w": 21, "h": 21},
    "cursor_ud": {"w": 18, "h": 19},
    "cursor_lr": {"w": 19, "h": 18},
    "cursor_exit": {"w": 33, "h": 13},
    "arrow_ud": {"w": 32, "h": 32},
    "arrow_ud2": {"w": 22, "h": 20},
    "arrow_fwd": {"w": 19, "h": 15},
    "pointer": {"w": 13, "h": 20},
    "check": {"w": 23, "h": 19},
    "meter": {"w": 90, "h": 15},
    "city": {"w": 566, "h": 471},
}
LEN_PALETTE = 0x300  # For background images, usually 0x2EB + padding
LEN_ARCHIVE_METADATA = 0x1400
LEN_ENTRY = 0x14
MAX_ENTRIES = LEN_ARCHIVE_METADATA // LEN_ENTRY  # = 256
PALETTE_NEEDLE = (
    b"\x00\x00\x00\x03"  # FIXME: Validate if palettes can start with other sequences
)

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s %(filename)s:%(lineno)s:%(funcName)s] %(message)s",
)


# Creates RGB pic from palette indexes
def convert_pic(filename, pic_struct):
    # Adjust array to simplify index calculation
    palette_chunk = pic_struct["palette_chunk"][4:]
    palette_chunk += b"\x00\x00\x00\x00"

    rgb_palette = []
    pixel_len = 3
    for pixel in [
        palette_chunk[i : i + pixel_len]
        for i in range(0, len(palette_chunk), pixel_len)
    ]:
        r = pixel[0]
        g = pixel[1]
        b = pixel[2]

        # Convert 6bit to 8bit.
        # References:
        # - [VGA Palette Conversion \\ VOGONS](https://www.vogons.org/viewtopic.php?t=38713)
        # - http://www.shikadi.net/moddingwiki/VGA_Palette#Conversion
        r = (r << 2) | (r >> 4)
        g = (g << 2) | (g >> 4)
        b = (b << 2) | (b >> 4)

        rgb_palette.append([r, g, b])

    w = pic_struct["w"]
    h = pic_struct["h"]
    data = numpy.zeros((h, w, 3), dtype=numpy.uint8)
    for i in range(w):
        for j in range(h):
            data[j, i] = rgb_palette[pic_struct["decompressed_chunk"][w * j + i]]

    image = Image.fromarray(data)
    image.save(filename)


# Creates new pic with xor'd palette indexes of two pics
def xor_pics(pic_struct, pic_struct_2):
    pic_struct_xor = copy.deepcopy(pic_struct)

    # FIXME: Assuming first pic has the largest w * h
    start_x = pic_struct_2["x"]
    start_y = pic_struct_2["y"]
    new_w = pic_struct_2["w"]
    new_h = pic_struct_2["h"]
    w = pic_struct["w"]
    for i in range(start_x, start_x + new_w, 1):
        for j in range(start_y, start_y + new_h, 1):
            v1 = pic_struct["decompressed_chunk"][w * j + i]
            v2 = pic_struct_2["decompressed_chunk"][
                new_w * (j - start_y) + (i - start_x)
            ]
            pic_struct_xor["decompressed_chunk"][w * j + i] = v1 ^ v2

    return pic_struct_xor


def decompress_chunk(filename, decompressed_len, decompression_cwd):
    input_file = Path(f"{chunk_path}/{filename}")
    output_file = Path(f"{decompress_path}/{filename}")
    if output_file.exists():
        logging.warning(
            f"Decompression skipped for {filename}, output already present!"
        )
        return

    cmd = [
        "wine",
        "python.exe",
        "./crusher_cli.py",
        "./CrusherS.dll",
        f"{input_file.absolute()}",
        f"-d {decompressed_len}",
        f"-o {output_file.absolute()}",
    ]
    try:
        proc = subprocess.run(cmd, cwd=decompression_cwd, capture_output=True)
        if not output_file.exists():
            logging.error(
                f"Decompression returned success for {filename}, but output not found!\n{proc.stdout}\n{proc.stderr}"
            )
    except subprocess.CalledProcessError as e:
        logging.error(
            f"Decompression failed (code {e.returncode}) for {filename}!\n{e.stdout}\n{e.stderr}"
        )


def closest_palette(offset, palettes):
    match = None
    offset_diff = sys.maxsize
    for palette_offset in palettes:
        candidate_offset_diff = abs(palette_offset - offset)
        if palette_offset <= offset and candidate_offset_diff < offset_diff:
            match = palette_offset
            offset_diff = candidate_offset_diff
        else:
            if not match:
                match = palette_offset
                logging.info(
                    f"Chunk @{hex(offset)} appears before first found palette @{hex(palette_offset)}, will use that one..."
                )
    return match


def guess_palette_offsets(input_bytes):
    palette_offsets = set()
    for x in re.finditer(re.escape(PALETTE_NEEDLE), input_bytes):
        offset = int(x.start())
        if offset < LEN_ARCHIVE_METADATA:
            continue
        ref_offset = (offset).to_bytes(16, byteorder="little").rstrip(b"\x00")
        matched = False
        for x2 in re.finditer(
            re.escape(ref_offset), input_bytes[:LEN_ARCHIVE_METADATA]
        ):
            matched = True
            break
        if matched:
            palette_offsets.add(offset)

    return palette_offsets


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data", type=str, help="Path for data file (e.g. MC001.PIC)")
    parser.add_argument("extract_dir", type=str, help="Path for extracted chunks (compressed)")
    parser.add_argument("decompress_dir", type=str, help="Path for decompressed chunks")
    parser.add_argument("convert_dir", type=str, help="Path for converted files (images)")
    parser.add_argument("crusher_dir", type=str, help="Path for crusher_cli")
    parsed_args = parser.parse_args()

    file = parsed_args.data
    chunk_dir = Path(parsed_args.extract_dir)
    chunk_dir.mkdir(parents=True, exist_ok=True)
    chunk_path = chunk_dir.absolute()
    decompress_dir = Path(parsed_args.decompress_dir)
    decompress_dir.mkdir(parents=True, exist_ok=True)
    decompress_path = decompress_dir.absolute()
    convert_dir = Path(parsed_args.convert_dir)
    convert_dir.mkdir(parents=True, exist_ok=True)
    convert_path = convert_dir.absolute()
    decompression_cwd = Path(parsed_args.crusher_dir).expanduser().absolute()

    input_bytes = None
    with open(file, "rb") as f:
        input_bytes = bytearray(f.read())

    palette_offsets = guess_palette_offsets(input_bytes)

    # Identify chunks
    candidates = {}
    for i in range(MAX_ENTRIES):
        offset = i * LEN_ENTRY
        block_start = struct.unpack("<L", input_bytes[offset : offset + 4])[0]
        if block_start == 0:
            continue  # Skip empty entries
        if i == MAX_ENTRIES - 1:
            next_block_start = len(input_bytes)
        else:
            next_offset = (i + 1) * LEN_ENTRY
            next_block_start = struct.unpack(
                "<L", input_bytes[next_offset : next_offset + 4]
            )[0]
            if next_block_start == 0:
                next_block_start = len(input_bytes)
        compressed_len = int(
            struct.unpack("<L", input_bytes[offset + 4 : offset + 8])[0]
        )
        has_palette_flag = (
            int(struct.unpack("<h", input_bytes[offset + 8 : offset + 10])[0]) == 0xFF00
        )
        x = int(struct.unpack("<h", input_bytes[offset + 10 : offset + 12])[0])
        y = int(struct.unpack("<h", input_bytes[offset + 12 : offset + 14])[0])
        w = int(struct.unpack("<h", input_bytes[offset + 14 : offset + 16])[0])
        h = int(struct.unpack("<h", input_bytes[offset + 16 : offset + 18])[0])
        pic_struct = {
            "compressed_len": compressed_len,
            "decompressed_chunk": None,
            "decompressed_len": w * h,
            "w": w,
            "h": h,
            "x": x,
            "y": y,
            "next_offset": next_block_start,
            "start_offset": block_start,
            "has_palette_flag": has_palette_flag,
            "palette_chunk": None,
            "palette_offset": None,
        }

        is_identified = False
        p = "p" if block_start in palette_offsets else " "
        for k, v in KNOWN_ENTRIES.items():
            if w == v["w"] and h == v["h"]:
                is_identified = True
                logging.debug(
                    f"Got {p} {k.rjust(12)},i:{i},i*len:{hex(i*LEN_ENTRY)},@{hex(block_start)},w:{w},h:{h}"
                )
                break
        if not is_identified:
            logging.debug(
                f"Got {p} ????????????,i:{i},i*len:{hex(i*LEN_ENTRY)},@{hex(block_start)},w:{w},h:{h}"
            )

        # Skip invalid sized chunks. During extraction,
        # assume they are contained in previous chunk.
        if w > 0 and h > 0:
            candidates[block_start] = pic_struct

    # Extract chunks
    palette_chunks = {}
    for c in candidates.values():
        # FIXME: These cases could imply a different application
        # of the palette which we don't support yet.
        start_offset = c["start_offset"]
        next_offset = c["next_offset"]
        if (
            next_offset - start_offset > c["compressed_len"]
            and start_offset not in palette_offsets
        ):
            logging.warning(
                "Compressed block length larger than expected but palette needle not matched!"
            )
            if c["has_palette_flag"]:
                logging.warning("Palette expected, adding to known offsets...")
                palette_offsets.add(start_offset)

        palette_offset = closest_palette(start_offset, palette_offsets)
        c["palette_offset"] = palette_offset
        if palette_offset not in palette_chunks:
            palette_block = input_bytes[palette_offset : palette_offset + LEN_PALETTE]
            with open(f"{chunk_path}/{file}-{hex(start_offset)}-pal", "wb") as f:
                f.write(palette_block)
            palette_chunks[palette_offset] = palette_block
        c["palette_chunk"] = palette_chunks[palette_offset]

        logging.info(
            f"Processing block {hex(start_offset)} with palette {hex(c['palette_offset'])}..."
        )
        block_start = start_offset
        if c["palette_offset"] == start_offset:
            block_start += LEN_PALETTE + 4

        block_end = next_offset
        block = input_bytes[block_start:block_end]
        basename = f"{file}-{hex(start_offset)}_{hex(next_offset)}"
        with open(f"{chunk_path}/{basename}", "wb") as f:
            f.write(block)
        decompress_chunk(basename, c["decompressed_len"], decompression_cwd)

        decompressed_file = Path(f"{decompress_path}/{basename}")
        if not decompressed_file.exists():
            logging.error(f"Conversion failed for {basename}!")
            with open("./error.log", "a") as f:
                f.write(f"{basename}, {c['w']}*{c['h']}\n")
            continue

        decompressed_filename = decompressed_file.absolute()
        c["decompressed_chunk"] = bytearray(open(decompressed_filename, "rb").read())

        converted_file = Path(convert_dir / f"{basename}.png")
        if converted_file.exists():
            logging.warning(
                f"Conversion skipped for {basename}, output already present!"
            )
            continue

        converted_filename = converted_file.absolute()
        convert_pic(converted_filename, c)

    # TODO:
    # Identify which entries correspond to
    # scenes with masked pics.
    #
    # The following graphics are composed to
    # create a mockup of all items in the
    # storage cabinet in the science lab,
    # including an unused third item.
    if file.endswith("MC006.PIC"):
        ca = candidates[0x215A9B]
        cb = candidates[0x226D28]
        cc = xor_pics(ca, cb)
        cb = candidates[0x22FAE1]
        cc = xor_pics(cc, cb)
        cb = candidates[0x2301C2]
        cc = xor_pics(cc, cb)
        cb = candidates[0x230E98]
        cc = xor_pics(cc, cb)
        basename = f"{file}-{hex(cc['start_offset'])}_{hex(cc['next_offset'])}-masked"
        converted_file = Path(convert_dir / f"{basename}.png")
        converted_filename = converted_file.absolute()
        convert_pic(converted_filename, cc)
