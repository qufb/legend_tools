#!/usr/bin/env python3

from pathlib import Path
from typing import List
import argparse
import logging
import struct

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s %(filename)s:%(lineno)s:%(funcName)s] %(message)s",
)


# uint16_t to int16_t
def s16(n: int) -> int:
    return ((n & 0xFFFF) ^ 0x8000) - 0x8000


def decompress_block(
    compressed_block: bytearray,
    lookup_entries: List[str],
    word_idxs_entries: List[str],
    words_entries: bytearray,
) -> str:
    output = ""
    shifts_counter = 8
    target_block_size = len(compressed_block)
    lookup_size = len(lookup_entries)
    lookup_val = lookup_size - 2
    for i in range(target_block_size):
        block_byte = compressed_block[i]
        for j in range(shifts_counter):
            logging.debug(f"- shifts_counter_i: {hex(j)}")
            logging.debug(f"- block_byte: {hex(block_byte)}")
            logging.debug(f"- lookup_val: {hex(lookup_val)}")

            if lookup_val >= 0xF000:
                signed_lookup_val = s16(lookup_val)
                decompressed_val = -(signed_lookup_val + 1)
                # end of text
                if decompressed_val == 0:
                    logging.debug("Got null byte, stopped parsing blocks.")
                    return output
                # literal character
                elif decompressed_val < 0x80:
                    decompressed_chr = chr(decompressed_val)
                    output += decompressed_chr
                    logging.debug(f"Added decompressed_chr: {decompressed_chr}")

                    lookup_val = lookup_size - 2
                # index to plaintext word
                elif decompressed_val - 0x80 < 0x80:
                    words_i = decompressed_val - 0x80
                    words_offset = int(word_idxs_entries[words_i], 16)
                    words_val = words_entries[words_offset]
                    # read word characters up to delimiter
                    while words_val != 0:
                        decompressed_chr = chr(words_val)
                        output += decompressed_chr
                        logging.debug(f"Added decompressed_chr: {decompressed_chr}")

                        words_offset += 1
                        words_val = words_entries[words_offset]

                    lookup_val = lookup_size - 2
                else:
                    logging.warning(
                        f"Expected signed lookup value to be < 0x80, was: {hex(decompressed_val - 0x80)}, original unsigned value: {hex(lookup_val)}"
                    )
                    return output

            first_bit = block_byte & 1
            lookup_offset = lookup_val | first_bit
            lookup_i = lookup_offset - lookup_size
            lookup_val = int(lookup_entries[lookup_i], 16)

            block_byte >>= 1

    return output


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data", type=str, help="Path for data file (e.g. MCSTR.DAT)")
    parser.add_argument("decompress_dir", type=str, help="Path for decompressed chunks")
    parsed_args = parser.parse_args()

    file = parsed_args.data
    decompress_dir = Path(parsed_args.decompress_dir)
    decompress_dir.mkdir(parents=True, exist_ok=True)

    input_bytes = None
    with open(file, "rb") as f:
        input_bytes = bytearray(f.read())

    logging.debug("\n### Section 0 (s0): snippet listing [0x0..0xda]")
    num_entries = struct.unpack("<H", input_bytes[0:2])[0]
    logging.debug(f"num_entries: {hex(num_entries)}")

    max_entry_len = 0
    snippet_entries = []
    offset = 0x2
    for i in range(num_entries):
        entry_len = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
        if entry_len > max_entry_len:
            max_entry_len = entry_len

        # Sum isn't needed for extraction,
        # it's only used to estimate memory allocations.
        entry_sum = struct.unpack("<H", input_bytes[offset + 2 : offset + 4])[0]

        entry_pad = struct.unpack("<H", input_bytes[offset + 4 : offset + 6])[0]
        if entry_pad != 0:
            logging.warning(f"Padding @{hex(i)} is not null: got {hex(entry_pad)}")

        snippet_entries.append(
            {
                "i": hex(i),
                "offset": hex(offset),
                "len": hex(entry_len),
                "sum": hex(entry_sum),
            }
        )

        offset += 6
    for a in snippet_entries:
        logging.debug(a)
    logging.debug(f"max_entry_len: {hex(max_entry_len)}")

    logging.debug("\n### Section 1 (s1): sizes for s0 entries [0xda..0x16a2]")
    block_len_entries = {}
    for i, entry in enumerate(snippet_entries):
        block_len_entries[hex(i)] = []
        for j in range(int(entry["len"], 16)):
            entry_value = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
            block_len_entries[hex(i)].append(hex(entry_value))

            offset += 2

    for a, b, in block_len_entries.items():
        logging.debug(f"{a}: {b}")

    logging.debug("\n### Section 2 (s2): lookup table [0x16a2..0x1a08]")
    num_s2_entries = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
    lookup_entries = []
    for i in range(num_s2_entries):
        offset += 2

        entry_value = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
        lookup_entries.append(hex(entry_value))
    logging.debug(f"num_s2_entries: {hex(num_s2_entries)}")
    logging.debug(lookup_entries)

    logging.debug("\n### Section 3 (s3): plaintext word indexes [0x1a08..0x1b0a]")
    offset += 2
    num_s3_entries = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
    word_idxs_entries = []
    for i in range(num_s3_entries):
        offset += 2

        entry_value = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
        word_idxs_entries.append(hex(entry_value))
    logging.debug(f"num_s3_entries: {hex(num_s3_entries)}")
    logging.debug(word_idxs_entries)

    logging.debug("\n### Section 4 (s4): plaintext words [0x1b0a..0x1e91]")
    offset += 2
    s4_entries_len = struct.unpack("<H", input_bytes[offset : offset + 2])[0]
    offset += 2
    words_entries = input_bytes[offset : offset + s4_entries_len]
    logging.debug(f"s4_entries_len: {hex(s4_entries_len)}")
    logging.debug(words_entries)

    logging.debug("\n### Section 5 (s5): compressed blocks [0x1e91..]")
    start_cx_blocks = offset + s4_entries_len
    logging.debug(f"start_cx_blocks: {hex(start_cx_blocks)}")

    target_offset = start_cx_blocks
    for i in range(num_entries):
        logging.info(f"Picking entry {hex(i)} snippets...")

        decompressed_strings = ""
        for j, block_len in enumerate(block_len_entries[hex(i)]):
            logging.debug(f"Picking snippet block {hex(j)} @ {hex(target_offset)}...")

            target_len = int(block_len, 16)
            compressed_block = input_bytes[target_offset : target_offset + target_len]

            decompressed_string = decompress_block(
                compressed_block, lookup_entries, word_idxs_entries, words_entries
            )
            decompressed_strings += f"{decompressed_string}\n"
            logging.debug(decompressed_string)

            # Seek to next block to parse
            target_offset += target_len

        decompressed_basename = Path(f"{file}-{hex(i)}.txt").stem + Path(f"{file}-{hex(i)}.txt").suffix
        decompressed_file = (decompress_dir / decompressed_basename).absolute()
        with open(decompressed_file, "w") as f:
            f.write(decompressed_strings)
